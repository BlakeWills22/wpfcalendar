﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFCalendar
{
    public static class MessageBus
    {
        private static Dictionary<Subscriber, object> Subscribers = new Dictionary<Subscriber, object>();
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        public static void Publish(IMessage message)
        {
            //Get the type to publish the messages too.
            var type = message.GetType();

            //Get a collection of all subscribers for that object type.
            IEnumerable<KeyValuePair<Subscriber, object>> result =
                Subscribers.Where(x => x.Key.ListenToType == type);

            //Execute the action on each object and include the message.
            var actionList = result.Select(x => x.Value).OfType<Action<IMessage>>().ToArray();
            foreach (var action in actionList)
            {
                action(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T" description="The Type of message to listen for."></typeparam>
        /// <param name="listener" description="The object listening for the message"></param>
        /// <param name="action" description="The action to execute when the message is raised."></param>
        public static void Subscribe<T>(object listener, Action<IMessage> action)
        {
            Subscribers.Add(new Subscriber(listener, typeof(T)), action);
        }

        class Subscriber
        {
            public Subscriber(object _listener, Type _type)
            {
                Listener = _listener;
                ListenToType = _type;
            }

            public object Listener { get; set; }
            public Type ListenToType { get; set; }
        }
    }
}
