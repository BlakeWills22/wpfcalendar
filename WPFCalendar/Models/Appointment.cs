﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPFCalendar.Models
{
    public class AppointmentModel
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int? Id { get; set; }

        [SQLite.NotNull]
        public string Subject { get; set; }

        [SQLite.NotNull]
        public string Location { get; set; }

        DateTime startDate = DateTime.Now.Date;
        [SQLite.NotNull]
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
            }
        }

        DateTime endDate = DateTime.Now.Date;
        [SQLite.NotNull]
        public DateTime EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Id:{0}, Subject={1}, Location={2}, StartDate={3}, EndDate={4}", Id, Subject, Location, StartDate, EndDate);
        }
    }
}
