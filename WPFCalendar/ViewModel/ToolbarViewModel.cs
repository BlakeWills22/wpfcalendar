﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.CompilerServices;
using WPFCalendar.Models;

namespace WPFCalendar.ViewModel
{
    public class ToolbarViewModel : BaseViewModel
    {
        public ToolbarViewModel()
        {
            //Popups
            MessageBus.Subscribe<PopupStateChangedMessage>(this, (message) =>
            {
                IsPopUpOpen = ((PopupStateChangedMessage)message).State;
            });
            openNewAptPopup = new RelayCommand((param) => OpenPopup(), () => !IsPopUpOpen);

            //Register to first event.
            MessageBus.Subscribe<MonthInViewChangedMessage>(this, (message) =>
            {
                CurrentMonth = ((MonthInViewChangedMessage)message).MonthInView;
            });

            navigateMonthsCommand = new RelayCommand(
            (param) =>
            {
                int months;
                int.TryParse(param.ToString(), out months);
                NavigateMonths(months);
            },
            () => true);
        }

        public RelayCommand openNewAptPopup { get; set; }
        public RelayCommand navigateMonthsCommand { get; set; }

        bool _isPopupOpen = false;
        public bool IsPopUpOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                NotifyPropertyChanged();
                openNewAptPopup.RaiseCanExecuteChanged();
            }
        }

        private DateTime _currentmonth;
        public DateTime CurrentMonth
        {
            get { return _currentmonth; }
            set
            {
                _currentmonth = value;
                NotifyPropertyChanged();
            }
        }


        void OpenPopup()
        {
            IsPopUpOpen = true;
            MessageBus.Publish(new PopupStateChangedMessage(IsPopUpOpen, new AppointmentModel()));
            openNewAptPopup.RaiseCanExecuteChanged();
        }

        void NavigateMonths(int monthsToAdd)
        {
            CurrentMonth = CurrentMonth.AddMonths(monthsToAdd);
            MessageBus.Publish(new MonthInViewChangedMessage(CurrentMonth));
        }
    }
}
