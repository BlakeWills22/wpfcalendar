﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCalendar.Models;

namespace WPFCalendar.ViewModel
{
    public class MonthCalendarViewModel : BaseViewModel
    {
        public MonthCalendarViewModel()
        {
            MessageBus.Subscribe<MonthInViewChangedMessage>(this, (message) =>
            {
                DateTime month = ((MonthInViewChangedMessage)message).MonthInView;
                //Ensure date is always the first.
                CurrentMonth = new DateTime(month.Year, month.Month, 1);
                GenerateDayMonthViewModels();
            });
        }

        private ObservableCollection<MonthDayViewModel> _days;
        public ObservableCollection<MonthDayViewModel> Days
        {
            get { return _days; }
            set
            {
                _days = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime _currentMonth;
        public DateTime CurrentMonth
        {
            get { return _currentMonth; }
            set
            {
                _currentMonth = value;
                NotifyPropertyChanged();
            }
        }

        public void GenerateDayMonthViewModels()
        {
            int daysInMonth = DateTime.DaysInMonth(CurrentMonth.Year, CurrentMonth.Month);
            FirstDayColumn = (int)CurrentMonth.DayOfWeek;

            DateTime date = CurrentMonth;
            Days = new ObservableCollection<MonthDayViewModel>();
            for (int i = 0; i < daysInMonth; i++)
            {
                var monthDayVM = new MonthDayViewModel() { Date = date };
                var apptList = new ObservableCollection<AppointmentModel>(SqliteEngine.Instance.Database.Table<AppointmentModel>().Where(x => x.StartDate == date).ToList());
                monthDayVM.AddAppointments(apptList);
                date = date.AddDays(1);
                Days.Add(monthDayVM);
            }
        }

        private int _firstDayColumn;
        public int FirstDayColumn
        {
            get { return _firstDayColumn; }
            set
            {
                _firstDayColumn = value;
                NotifyPropertyChanged();
            }
        }
    }
}
