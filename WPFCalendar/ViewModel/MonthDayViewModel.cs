﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCalendar.Models;
using System.Windows.Media;
using System.Collections.Specialized;

namespace WPFCalendar.ViewModel
{
    public class MonthDayViewModel : BaseViewModel
    {
        public MonthDayViewModel()
        {
            //Hook up commands
            ListItemClickedCommand = new RelayCommand(
                (param) =>
                {
                    ListItemClick((int)param);
                },
                () => true
            );

            //Subscribe to events.
            MessageBus.Subscribe<NewAppointmentCreatedMessage>(this, (message) =>
            {
                var newAptMessage = ((NewAppointmentCreatedMessage)message);
                //TODO: Update if condition to between start and end date.
                if (newAptMessage.NewAppointment.StartDate == Date)
                {
                    Appointments.Add(newAptMessage.NewAppointment);
                }
            });

            MessageBus.Subscribe<AppointmentUpdatedMessage>(this, (message) =>
            {
                var updatedAppointment = ((AppointmentUpdatedMessage)message).UpdatedAppointment;
                var originalAppointment = ((AppointmentUpdatedMessage)message).OriginalAppointment;
                UpdateAppointments(updatedAppointment, originalAppointment);
            });

            MessageBus.Subscribe<AppointmentDeletedMessage>(this, (message) =>
            {
                var deletedAptMessage = ((AppointmentDeletedMessage)message);
                if (deletedAptMessage.DeletedAppointment.StartDate == Date)
                {
                    Appointments.Remove(deletedAptMessage.DeletedAppointment);
                }
            });
        }

        public RelayCommand ListItemClickedCommand { get; set; }

        DateTime date;
        public DateTime Date
        {
            get { return date; }
            set
            {
                DateTime _date = date;
                date = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<AppointmentModel> appointments;
        public ObservableCollection<AppointmentModel> Appointments
        {
            get { return appointments; }
            private set
            {
                appointments = value;
                appointments.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) =>
                {
                    NotifyPropertyChanged();
                };
                NotifyPropertyChanged();
            }
        }

        public Brush BackgroundColor
        {
            get
            {
                return (Date == DateTime.Now.Date) ?
                    (SolidColorBrush)App.Current.Resources["TodayColor"]
                    : Brushes.White;
            }
        }

        public void AddAppointments(ObservableCollection<AppointmentModel> appointmentList)
        {
            Appointments = appointmentList;
        }

        private void ListItemClick(int selectedIndex)
        {
            var appointment = Appointments.SingleOrDefault(x => x.Id == selectedIndex);
            MessageBus.Publish(new PopupStateChangedMessage(true, appointment));
        }

        private void UpdateAppointments(AppointmentModel updatedAppointment, AppointmentModel originalAppointment)
        {
            if (updatedAppointment.StartDate == Date)
            {
                //Get the appointment object to update.
                var aptToUpdate = Appointments.SingleOrDefault(x => x.Id.Value == updatedAppointment.Id);
                if (aptToUpdate != null) //Appointment won't be null if the original start date is the same as the updated start date.
                {
                    int index = Appointments.IndexOf(aptToUpdate);
                    Appointments[index] = updatedAppointment;
                }
                else //Handle the case where a user moves the appointment from one day to another. (The appointment won't be in this days list).
                {
                    Appointments.Add(updatedAppointment);
                }
            }

            //Appointment has been moved to a different day, remove it.
            if (originalAppointment.StartDate == Date && updatedAppointment.StartDate != Date)
            {
                Appointments.Remove(originalAppointment);
            }
        }
    }
}
