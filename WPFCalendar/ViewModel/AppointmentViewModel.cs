﻿using System;
using WPFCalendar.Models;
using System.ComponentModel.DataAnnotations;

namespace WPFCalendar.ViewModel
{
    public class AppointmentViewModel : BaseViewModel
    {
        public AppointmentViewModel(AppointmentModel appointment)
        {
            //Hook up Commands
            SaveCommand = new RelayCommand(
                (param) => SaveAppointment(),
                () => !HasErrors
            );

            ClosePopupCommand = new RelayCommand(
                (param) => ClosePopup(),
                () => true);

            DeleteCommand = new RelayCommand(
                (param) => DeleteAppointment(),
                () => Id.HasValue);

            //Add Data
            if (appointment != null)
            {
                StartDate = appointment.StartDate;
                EndDate = appointment.EndDate;
                Subject = appointment.Subject;
                Location = appointment.Location;
                Id = appointment.Id;

                originalAppointment = appointment;
            }
        }

        //Object to hold the original appointment.
        AppointmentModel originalAppointment;

        private DateTime startDate = DateTime.Now;
        [DateValidator(DateValidatorType.LessThanOrEqual, "EndDate")]
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                ValidateAndRaiseEvents(startDate);
                ValidateProperty("EndDate", EndDate);
                SaveCommand.RaiseCanExecuteChanged();
            }
        }

        private DateTime endDate;
        [DateValidator(DateValidatorType.GreaterThanOrEqual, "StartDate")]
        public DateTime EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
                ValidateAndRaiseEvents(endDate);
                ValidateProperty(nameof(StartDate), startDate);
                SaveCommand.RaiseCanExecuteChanged();
            }
        }

        private string subject;
        [Required]
        public string Subject
        {
            get { return subject; }
            set
            {
                subject = value;
                ValidateAndRaiseEvents(subject);
                SaveCommand.RaiseCanExecuteChanged();
            }
        }

        private string location;
        [Required]
        public string Location
        {
            get { return location; }
            set
            {
                location = value;
                ValidateAndRaiseEvents(location);
                SaveCommand.RaiseCanExecuteChanged();
            }
        }


        private int? id;
        public int? Id
        {
            get { return id; }
            set
            {
                int _id = (id.HasValue) ? id.Value : 0;
                id = value;
                NotifyPropertyChanged();
                DeleteCommand.RaiseCanExecuteChanged();
            }
        }

        #region Commands
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        public RelayCommand ClosePopupCommand { get; set; }

        private AppointmentModel generateAppointment()
        {
            return new AppointmentModel()
            {
                Id = this.Id,
                Subject = this.Subject,
                Location = this.Location,
                StartDate = this.StartDate,
                EndDate = this.EndDate
            };
        }

        void SaveAppointment()
        {
            var appointment = generateAppointment();
            if (appointment.Id.HasValue)
            {
                SqliteEngine.Instance.Database.Update(appointment);
                MessageBus.Publish(new AppointmentUpdatedMessage(appointment, originalAppointment));
            }
            else
            {
                SqliteEngine.Instance.Database.Insert(appointment);
                MessageBus.Publish(new NewAppointmentCreatedMessage(appointment));
            }
            ClosePopup();
        }

        void ClosePopup()
        {
            MessageBus.Publish(new PopupStateChangedMessage(false, null));
        }

        void DeleteAppointment()
        {
            var appointment = generateAppointment();
            SqliteEngine.Instance.Database.Delete(appointment);
            MessageBus.Publish(new AppointmentDeletedMessage(appointment));
            ClosePopup();
        }
        #endregion
    }
}
