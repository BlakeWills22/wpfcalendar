﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using WPFCalendar.ViewModel;

namespace WPFCalendar.Views
{
    /// <summary>
    /// Interaction logic for AppointmentView.xaml
    /// </summary>
    public partial class AppointmentView : UserControl
    {
        public AppointmentView()
        {
            InitializeComponent();
            MessageBus.Subscribe<PopupStateChangedMessage>(this, (message) =>
            {
                var popupMessage = ((PopupStateChangedMessage)message);
                if (popupMessage.State)
                {
                    this.DataContext = new AppointmentViewModel(popupMessage.AppointmentObject);
                }
            });
        }
    }
}
