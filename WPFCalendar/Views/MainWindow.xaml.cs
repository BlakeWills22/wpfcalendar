﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFCalendar.ViewModel;
using WPFCalendar;
using MahApps.Metro.Controls;

namespace WPFCalendar.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            this.DataContext = this;
            InitializeComponent();
            MessageBus.Subscribe<PopupStateChangedMessage>(this, (message) => {
                IsPopUpOpen = ((PopupStateChangedMessage)message).State;
            });
            MessageBus.Publish(new MonthInViewChangedMessage(DateTime.Now));
        }

        // Using a DependencyProperty as the backing store for IsPopUpOpen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPopUpOpenProperty =
            DependencyProperty.RegisterAttached("IsPopUpOpen", typeof(bool), typeof(MainWindow), new PropertyMetadata(false));

        public bool IsPopUpOpen
        {
            get { return (bool)this.GetValue(IsPopUpOpenProperty); }
            set { SetValue(IsPopUpOpenProperty, value); }
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AppParameters.WindowHeight = Height - TitlebarHeight;
            AppParameters.WindowWidth = Width;
        }
    }
}
