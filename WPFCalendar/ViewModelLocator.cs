﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFCalendar
{
    public static class ViewModelLocator
    {
        public static bool GetAutoWireViewProperty(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoWireViewPropertyProperty);
        }

        public static void SetAutoWireViewProperty(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoWireViewPropertyProperty, value);
        }

        // Using a DependencyProperty as the backing store for AutoWireViewProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoWireViewPropertyProperty =
            DependencyProperty.RegisterAttached("AutoWireViewProperty", typeof(bool), typeof(ViewModelLocator), new PropertyMetadata(false, AutoWireViewModelChanged));

        private static void AutoWireViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            var viewType = d.GetType();
            var viewTypeName = viewType.FullName;
            var viewModelTypeName = viewTypeName.Replace(".Views.", ".ViewModel.") + "Model";
            var viewModelType = Type.GetType(viewModelTypeName);
            var viewModel = Activator.CreateInstance(viewModelType);
            ((FrameworkElement)d).DataContext = viewModel;
        }
    }
}
