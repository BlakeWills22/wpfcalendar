﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFCalendar
{
    public class RelayCommand : ICommand
    {
        public RelayCommand(Action<object> _executeCmd, Func<bool> _canExecute)
        {
            execute = _executeCmd;
            canExecute = _canExecute;
        }
        //What to execute when the command is executed
        private Action<object> execute;
        //Predicate to evaluate to determine if the command can be executed
        private Func<bool> canExecute;
        //Event to fire when something happens that may affect if the command can be executed.
        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            execute(parameter);
        }

        public bool CanExecute(object parameter)
        {
            return canExecute();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }


    }
}
