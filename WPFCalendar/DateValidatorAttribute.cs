﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFCalendar
{
    public sealed class DateValidatorAttribute : ValidationAttribute
    {
        private readonly string testedPropertyName;
        private readonly DateValidatorType validatorType;

        public DateValidatorAttribute(DateValidatorType _validatorType, string _testedPropertyName)
        {
            testedPropertyName = _testedPropertyName;
            validatorType = _validatorType;
        }

        //End Point for Unit Tests
        public ValidationResult CheckIsValid(object value, ValidationContext validationContext)
        {
            return IsValid(value, validationContext);
        }

        //Actual validation method.
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            #region defaultChecks
            var propertyTestedInfo = validationContext.ObjectType.GetProperty(testedPropertyName);
            if (propertyTestedInfo == null)
            {
                return new ValidationResult(string.Format("Unknown Property {0}", testedPropertyName));
            }

            var propertyTestedValue = propertyTestedInfo.GetValue(validationContext.ObjectInstance, null);

            if (value == null || !(value is DateTime))
            {
                return new ValidationResult("Value is null or not a DateTime");
            }

            if (propertyTestedValue == null || !(propertyTestedValue is DateTime))
            {
                return new ValidationResult("Comparison Value is null or not a DateTime");
            }
            #endregion

            //Passed default checks (meaning both values are datetimes and not null), cast both values to DateTime to avoid doing this on every check.
            DateTime AttributeValue = (DateTime)value;
            DateTime OtherValue = (DateTime)propertyTestedValue;

            //Greater Than
            if (AttributeValue > OtherValue && validatorType == DateValidatorType.GreaterThan)
            {
                return ValidationResult.Success;
            }
            //Greater Than Or Equal
            if (AttributeValue >= OtherValue && validatorType == DateValidatorType.GreaterThanOrEqual)
            {
                return ValidationResult.Success;
            }

            //Less Than
            if (AttributeValue < OtherValue && validatorType == DateValidatorType.LessThan)
            {
                return ValidationResult.Success;
            }

            //Less Than Or Equal
            if (AttributeValue <= OtherValue && validatorType == DateValidatorType.LessThanOrEqual)
            {
                return ValidationResult.Success;
            }

            //Equal
            if (AttributeValue == OtherValue && validatorType == DateValidatorType.Equals)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(BuildFailedErrorMessage(
                validationContext.MemberName.AddSpacesToSentence(false), testedPropertyName.AddSpacesToSentence(false)));
        }

        string BuildFailedErrorMessage(string propertyName, string comparisonPropertyName)
        {
            string clause = Enum.GetName(typeof(DateValidatorType), validatorType).AddSpacesToSentence(false);
            string toToOrNotToToo = (clause.Contains("Or Equal")) ? " too " : " ";
            return string.Format("{0} must be {1}{2}{3}", propertyName, clause, toToOrNotToToo, comparisonPropertyName);
        }
    }

    public enum DateValidatorType
    {
        GreaterThan = 1,
        GreaterThanOrEqual = 2,
        LessThan = 3,
        LessThanOrEqual = 4,
        Equals = 5
    }
}
