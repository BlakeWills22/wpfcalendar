﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCalendar.Models;

namespace WPFCalendar
{
    public class SqliteEngine
    {
        private SqliteEngine() { }

        private static SqliteEngine instance;
        public static SqliteEngine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SqliteEngine();
                    instance.GetConnection();
                }
                return instance;
            }
        }
        public SQLiteConnection Database { get; private set; }

        private void GetConnection()
        {
            Database = new SQLiteConnection(string.Format("{0}WPFCalendar.sqlite", AppDomain.CurrentDomain.BaseDirectory.ToString()));
            Database.CreateTable<AppointmentModel>();
        }
    }
}
