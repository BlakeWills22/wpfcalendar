﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCalendar.Models;

namespace WPFCalendar
{

    /// <summary>
    /// An empty interface to ensures messages being broadcast via the MessageBus are indeed messages.
    /// </summary>
    public interface IMessage { }

    public class PopupStateChangedMessage : IMessage
    {
        public PopupStateChangedMessage(bool state, AppointmentModel appointment)
        {
            State = state;
            AppointmentObject = appointment;
        }
        public bool State { get; private set; }
        public AppointmentModel AppointmentObject { get; set; }
    }

    public class NewAppointmentCreatedMessage : IMessage
    {
        public NewAppointmentCreatedMessage(AppointmentModel appointment)
        {
            NewAppointment = appointment;
        }
        public AppointmentModel NewAppointment { get; private set; }
    }

    public class AppointmentUpdatedMessage : IMessage
    {
        public AppointmentUpdatedMessage(AppointmentModel updatedAppointment, AppointmentModel originalAppointment)
        {
            UpdatedAppointment = updatedAppointment;
            OriginalAppointment = originalAppointment;
        }

        public AppointmentModel UpdatedAppointment { get; set; }
        public AppointmentModel OriginalAppointment { get; set; }
    }

    public class AppointmentDeletedMessage : IMessage
    {
        public AppointmentDeletedMessage(AppointmentModel appointment)
        {
            DeletedAppointment = appointment;
        }

        public AppointmentModel DeletedAppointment { get; private set; }
    }

    public class MonthInViewChangedMessage : IMessage
    {
        public MonthInViewChangedMessage(DateTime monthInView)
        {
            MonthInView = monthInView;
        }

        public DateTime MonthInView { get; private set; }
    }
}
