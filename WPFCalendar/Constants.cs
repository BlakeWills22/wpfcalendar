﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFCalendar
{
    public static class AppParameters
    {
        public static string[] DaysOfWeek {
            get
            {
                //TODO: Make dynamic based on First Day of Week Setting.
                return new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            }
        }

        public static double WindowHeight { get; set; }
        public static double WindowWidth { get; set; }
        public static double WindowTitleBarHeight { get; set; }
    }
}
