﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCalendar;

namespace WPFCalendarUnitTests
{
    public class DateValidatorEqualsTestModel
    {
        [DateValidator(DateValidatorType.Equals, nameof(ComparisonValue))]
        public DateTime? Value { get; set; }

        public DateTime? ComparisonValue { get; set; }
    }

    public class DateValidatorGreaterThanTestModel
    {
        [DateValidator(DateValidatorType.GreaterThan, nameof(ComparisonValue))]
        public DateTime? Value { get; set; }

        public DateTime? ComparisonValue { get; set; }
    }

    public class DateValidatorGreaterThanOrEqualTestModel
    {
        [DateValidator(DateValidatorType.GreaterThanOrEqual, nameof(ComparisonValue))]
        public DateTime? Value { get; set; }

        public DateTime? ComparisonValue { get; set; }
    }

    public class DateValidatorLessThanTestModel
    {
        [DateValidator(DateValidatorType.LessThan, nameof(ComparisonValue))]
        public DateTime? Value { get; set; }

        public DateTime? ComparisonValue { get; set; }
    }

    public class DateValidatorLessThanOrEqualTestModel
    {
        [DateValidator(DateValidatorType.LessThanOrEqual, nameof(ComparisonValue))]
        public DateTime? Value { get; set; }

        public DateTime? ComparisonValue { get; set; }
    }

    public class DateValidatorNotDateTimeTestModel
    {
        [DateValidator(DateValidatorType.LessThanOrEqual, nameof(ComparisonValue))]
        public object Value { get; set; }

        public object ComparisonValue { get; set; }
    }
}
