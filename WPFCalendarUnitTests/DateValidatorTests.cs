﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFCalendar;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WPFCalendarUnitTests
{
    [TestClass]
    public class DateValidatorAttributeTests
    {
        /// <summary>
        /// Ensures validation fails when the value is null.
        /// </summary>
        [TestMethod]
        public void testValidation_NullValue_Fail()
        {
            var testModel = new DateValidatorEqualsTestModel()
            {
                Value = null,
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results[0]);
        }

        /// <summary>
        /// Ensure validation fails when the value is not a DateTime.
        /// </summary>
        [TestMethod]
        public void testValidation_ValueNotDateTime_Fail()
        {
            var testModel = new DateValidatorNotDateTimeTestModel()
            {
                Value = 1,
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results[0]);
        }

        /// <summary>
        /// Ensures validation fails when the Comparison value is null.
        /// </summary>
        [TestMethod]
        public void testValidation_NullComparisonValue_Fail()
        {
            var testModel = new DateValidatorEqualsTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = null
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results[0]);
        }

        /// <summary>
        /// Ensures validation fails when the Comparison Value is not a DateTime.
        /// </summary>
        [TestMethod]
        public void testValidation_ComparisonValueNotDateTime_Fail()
        {
            var testModel = new DateValidatorNotDateTimeTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = 1
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results[0]);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is greater than the comparison value, and ValidationType.GreaterThan is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanValidation_ValueGreaterThanComparisonValue_Pass()
        {
            var testModel = new DateValidatorGreaterThanTestModel()
            {
                Value = new DateTime(2016, 01, 02),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is not successfull when the value is less than the comparsion value, and ValidationType.GreaterThan is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanValidation_ValueLessThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorGreaterThanTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 02)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is not successfull when the value is equal to the comparsion value, and ValidationType.GreaterThan is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanValidation_ValueEqualToComparisonValue_Fail()
        {
            var testModel = new DateValidatorGreaterThanTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is greater than the comparsion value, and ValidationType.GreaterThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanOrEqualValidation_ValueGreaterThanComparisonValue_Pass()
        {
            var testModel = new DateValidatorGreaterThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 02),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is equal to the comparsion value, and ValidationType.GreaterThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanOrEqualValidation_ValueEqualToComparisonValue_Pass()
        {
            var testModel = new DateValidatorGreaterThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is less than the comparsion value, and ValidationType.GreaterThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testGreaterThanOrEqualValidation_ValueLessThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorGreaterThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 02)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is less than the comparsion value, and ValidationType.LessThan is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanValidation_ValueLessThanComparisonValue_Pass()
        {
            var testModel = new DateValidatorLessThanTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 02)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is greater than the comparsion value, and ValidationType.LessThan is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanValidation_ValueGreaterThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorLessThanTestModel()
            {
                Value = new DateTime(2016, 01, 02),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is equal to the comparsion value, and ValidationType.LessThan is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanValidation_ValueEqualToComparisonValue_Fail()
        {
            var testModel = new DateValidatorLessThanTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is less than the comparsion value, and ValidationType.LessThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanOrEqualValidation_ValueLessThanComparisonValue_Pass()
        {
            var testModel = new DateValidatorLessThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 02)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is equal to the comparsion value, and ValidationType.LessThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanOrEqualValidation_ValueEqualToComparisonValue_Pass()
        {
            var testModel = new DateValidatorLessThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is greater than the comparsion value, and ValidationType.LessThanOrEqual is specified.
        /// </summary>
        [TestMethod]
        public void testLessThanOrEqualValidation_ValueGreaterThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorLessThanOrEqualTestModel()
            {
                Value = new DateTime(2016, 01, 02),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation is successfull when the value is equal to the comparsion value, and ValidationType.Equals is specified.
        /// </summary>
        [TestMethod]
        public void testEqualToValidation_ValueEqualToComparisonValue_Pass()
        {
            var testModel = new DateValidatorEqualsTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is greater than the comparsion value, and ValidationType.Equals is specified.
        /// </summary>
        [TestMethod]
        public void testEqualToValidation_ValueGreaterThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorEqualsTestModel()
            {
                Value = new DateTime(2016, 01, 02),
                ComparisonValue = new DateTime(2016, 01, 01)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }

        /// <summary>
        /// Ensures validation fails when the value is less than the comparsion value, and ValidationType.Equals is specified.
        /// </summary>
        [TestMethod]
        public void testEqualToValidation_ValueLessThanComparisonValue_Fail()
        {
            var testModel = new DateValidatorEqualsTestModel()
            {
                Value = new DateTime(2016, 01, 01),
                ComparisonValue = new DateTime(2016, 01, 02)
            };
            List<ValidationResult> results = ValidateObject(testModel);
            Assert.AreNotEqual(ValidationResult.Success, results);
        }


        /// <summary>
        /// Attempts to validate the object and returns a collection of ValidationResults listing errors, or null if validation was successfull.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<ValidationResult> ValidateObject(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            //Needs to return null if successfull so we can Assert.Equal(ValidationResult.Success... (Which is a constant null value)
            return validationResults.Any() ? validationResults : null;
        }
    }
}
